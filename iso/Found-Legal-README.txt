= License =

The Found Operating System is a compilation of software packages, 
each under its own license. The compilation itself is released 
under the MIT license (see the file LICENSE). However, this 
compilation license does not supersede the licenses of code and 
content contained in the Found Operating System, which conform 
to the legal guidelines described at
https://gitee.com/foundos_1

= Source Availability =

The accompanying Found Linux 39 release includes copyrighted
software that is licensed under the GNU General Public License and
other licenses. You may obtain the complete machine-readable source
code corresponding to portions of this release by sending a request, including the version number to:

https://gitee.com/foundos_1

